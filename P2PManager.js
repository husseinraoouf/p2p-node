var net = require('net');
const fetch = require('node-fetch');
const EventEmitter = require('events');

const serverUrl = 'http://localhost:5050';

module.exports = class P2PManager extends EventEmitter {
    constructor(port) {
        super();    
        this.port = port;
        this._connections = [];
    }

    _onData(socket, data) {
        const JsonData = JSON.parse(data.toString());
        this.emit(JsonData.action, socket, JsonData.payload);
    }

    startSocketLisenter() {
        const server = net.createServer((socket) => {

            socket.name = socket.remoteAddress + ":" + socket.remotePort;

            this._connections.push(socket);

            socket.on('error', (err) => console.log(err));
            socket.on('data', data => this._onData(socket, data));
            socket.on('end', hadError => {
                this._connections.splice(this._connections.indexOf(socket), 1);
                this.emit('end', socket, hadError);
            })

        });

        server.listen(this.port);
        server.on('listening', async () => {
            await this.annouceConnect(this.port)
            this.emit("listening");
        });
    }
    
    async annouceConnect() {
        await fetch(`${serverUrl}/connect/${this.port}`);
    }

    async annouceDisconnect() {
        await fetch(`${serverUrl}/disconnect/${this.port}`);
    }

    async connect() {
        const peers = await this._getAllPeers();
        await this._connectToPeers(peers);
    }

    async _getAllPeers() {
        const response = await fetch(`${serverUrl}/${this.port}`);
        const peers = await response.json();
        return peers;
    }
    
    async _connectToPeers(peers) {
        let first = true;
    
        const promisise = [];
    
        peers.forEach((peer) => {
            const ip = peer.address.slice(0, peer.address.lastIndexOf(':'));
            const port = peer.address.slice(peer.address.lastIndexOf(':')+1);
            
            const socket = net.Socket();
            socket.connect(port, ip);
            
            promisise.push(new Promise((resolve, reject) => {
                socket.on('connect', () => {
                    if (first) {
                        first = false;
                        this.emit('firstConnect', socket);
                    }

                    socket.name = socket.remoteAddress + ":" + socket.remotePort;
                    this._connections.push(socket);
                    resolve();
                });
                
                socket.on('error', (err) => {
                    reject();
                    console.log(err);
                });
                socket.on('data', data => this._onData(socket, data));
                socket.on('end', hadError => {
                    this._connections.splice(this._connections.indexOf(socket), 1);
                    this.emit('end', socket, hadError);
                });
            }))
    
        });
        await Promise.all(promisise).catch(() => console.log('error'));
    }


    broadcastData(action, data) {
        for (const socket of this._connections) {
            socket.write(JSON.stringify({action, payload: data}));
        }
    }


    send(socket, action, data) {
        socket.write(JSON.stringify({action, payload: data}));
    }
}







