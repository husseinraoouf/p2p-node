const faker = require('faker');
const ULID = require('ulid');
const EventEmitter = require('events');


module.exports = class DataGenerator extends EventEmitter {

    constructor(interval) {
        super()
        this.interval = interval;
        this.startDataGeneration();
    }


    pauseDataGeneration() {
        clearInterval(this.intervalId);
    }

    startDataGeneration() {
        this.intervalId = setInterval(() => {
        
            this.emit('newData', [
            {_id: ULID.ulid(), data: faker.name.findName()},
            {_id: ULID.ulid(), data: faker.name.findName()},
            {_id: ULID.ulid(), data: faker.name.findName()},
            {_id: ULID.ulid(), data: faker.name.findName()},
            {_id: ULID.ulid(), data: faker.name.findName()}]);


        }, this.interval);
    }
}




