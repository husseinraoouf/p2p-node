const P2PManager = require('./P2PManager.js');
const DataGenerator = require('./DataGenerator.js');
const DataStore = require('./DataStore.js');

const start = async () => {
    const port = process.argv[2]

    const store = new DataStore(port);
    const generator = new DataGenerator(10000);
    const p2pManager = new P2PManager(port);

    store.on('resumeDataGeneration', () => {
        console.log('resumeDataGeneration');
        generator.startDataGeneration();
        p2pManager.broadcastData('resumeDataGeneration');
    });

    store.on('pauseDataGeneration', () => {
        console.log('pauseDataGeneration');
        generator.pauseDataGeneration();
        p2pManager.broadcastData('pauseDataGeneration');
    });

    p2pManager.on('resumeDataGeneration', () => {
        console.log('resumeDataGeneration');
        generator.startDataGeneration();
    });

    p2pManager.on('pauseDataGeneration', () => {
        console.log('pauseDataGeneration');
        generator.pauseDataGeneration();
    });

    p2pManager.on('listening', () => console.log(`Socket Listens on port ${port}`));

    p2pManager.on('firstConnect', async (socket) => {
        const count = await store.count();
        p2pManager.send(socket, 'requestMissingData', { after: count });
    })

    p2pManager.on('requestMissingData', async (socket, data) => {
        const missingData = await store.getDataAfter(data.after);
        p2pManager.send(socket, 'sendMissingData', missingData);
    });

    p2pManager.on('sendMissingData', async (socekt, data) => {
        console.log('Missing Data: ', data);
        store.addData(data);
    })

    p2pManager.on('newData', async (socekt, data) => {
        console.log('recived', data);
        store.addData(data);
    });


    generator.on('newData', async (data) => {
        console.log('generated', data);
        store.addData(data);
        p2pManager.broadcastData('newData', data);
    });

    p2pManager.on('msg', (socket, data) => {
        console.log(`${socket.name}: ${data}`);
    });

    p2pManager.on('end', (socket, hadError) => {
        console.log(`${socket.name}: socket out`);
    });



    p2pManager.startSocketLisenter();
    await p2pManager.connect();
    
    p2pManager.broadcastData('msg', `Hello From port ${port}`);


    process.stdin.resume();//so the program will not close instantly

    async function exitHandler() {
        await p2pManager.annouceDisconnect();
        process.exit();
    }

    //do something when app is closing
    process.on('exit', exitHandler);
    //catches ctrl+c event
    process.on('SIGINT', exitHandler);
    // catches "kill pid" (for example: nodemon restart)
    process.on('SIGUSR1', exitHandler);
    process.on('SIGUSR2', exitHandler);
    //catches uncaught exceptions
    process.on('uncaughtException', exitHandler);
}


start();
