const Datastore = require('nedb-promises');
const EventEmitter = require('events');

module.exports = class DataStore extends EventEmitter {
    
    constructor(DBName) {
        super();
        this.db = Datastore.create(`./${DBName}.db`);
        this.db.load();

        this.queue = [];
        this.dataGenerationPauesd = false;

        setInterval(() => {
            if (this.queue.length == 0) return;

            if(this.dataGenerationPauesd && this.queue.length <  10) {
                this.dataGenerationPauesd = false;
                this.emit('resumeDataGeneration');
                return;
            }
    
            const currentRecord = this.queue[0];
            this.queue.splice(0,1);
        
            this.db.insert(currentRecord);
        }, 500);
    }
    
    count() {
        return this.db.count();
    }

    addData(data) {
        if (!Array.isArray(data)) {
            data = [data];
        }

        this.queue = [...this.queue, ...data];

        if (!this.dataGenerationPauesd && this.queue.length > 100) {
            this.dataGenerationPauesd = true;
            this.emit('pauseDataGeneration');
        }
    }    

    async getDataAfter(after) {
        const missingData = await this.db.find().sort({ _id: 1 }).skip(after).exec();
        return [...missingData, ...this.queue];
    }

}




